<!DOCTYPE html>
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<html lang="en-US" xml:lang="en-US" xmlns= "http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="google" content="notranslate">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<title><tiles:getAsString name="title" /></title>
	<link rel="shortcut icon" href="/resources/img/favicon.ico">
	<!-- O'Reilly BS -->
	<link rel="stylesheet" href="<c:url value='/resources/css/oreillybs-4.0.0r1.min.css'/>">
	<!-- O'Reilly JS -->
	<script src="<c:url value='/resources/js/oreillyjs/1.1.40/orly.js' />"></script>
</head>
<body>
	<div id="bodyContentTile" class="container">
		<div class="row">
			<div class="col-12">
				<orly-alert-mgr id="alerts"></orly-alert-mgr>
				<tiles:insertAttribute name="body" />
			</div>
		</div>
	</div>
</body>
</html>
