<%@ include file="/WEB-INF/layouts/include.jsp"%>
<c:url var="linkToHome" value="/" />

<div class="container-error">
	<h1>OOPS! There was an error!</h1>
	<a class="btn btn-primary" href="<c:url value='/' />">Home</a>
</div>
