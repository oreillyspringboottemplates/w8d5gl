package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class W8d5glApplication {

	public static void main(String[] args) {
		SpringApplication.run(W8d5glApplication.class, args);
	}

}
