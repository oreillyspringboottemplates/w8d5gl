package com.oreillyauto.controllers;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.dto.ExampleStrings;

@Controller
public class ExampleController {

    @GetMapping(path="/example")
    public String getExample(HttpServletResponse resp) {
        resp.setStatus(403);
        return "example";
    }
    
    @ResponseBody
    @GetMapping(value = { "/example/getHelloWorld" })
    public ExampleStrings getHelloWorldJSON()  {
    	ExampleStrings exampleStrings = new ExampleStrings();
    	exampleStrings.setString1("Hello");
    	exampleStrings.setString2("World");
    	return exampleStrings;
    }


}