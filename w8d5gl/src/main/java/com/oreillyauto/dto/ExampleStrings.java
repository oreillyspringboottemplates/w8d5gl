package com.oreillyauto.dto;

import java.util.Objects;

public class ExampleStrings {
private String string1;
private String string2;
public String getString1() {
	return string1;
}
public void setString1(String string1) {
	this.string1 = string1;
}
public String getString2() {
	return string2;
}
public void setString2(String string2) {
	this.string2 = string2;
}
@Override
public int hashCode() {
	return Objects.hash(string1, string2);
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	ExampleStrings other = (ExampleStrings) obj;
	return Objects.equals(string1, other.string1) && Objects.equals(string2, other.string2);
}
@Override
public String toString() {
	return "ExampleStrings [string1=" + string1 + ", string2=" + string2 + "]";
}

}
